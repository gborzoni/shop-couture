var fns = {
	tabs: function(){
		$(".tabNav").on("click", ".tabLink", function() {
	        var ref = $(this).attr("data-ref");
	        $(".tabLink, .tabContent").removeClass("active");
	        $(this).addClass("active");
	        $(".tabsItems").find("."+ref).addClass("active");
	    });  
	},

	isMobile: function(){
		var userAgent = navigator.userAgent.toLowerCase();
		if( userAgent.search(/(android|avantgo|blackberry|iemobile|nokia|lumia|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 ){			
			return true;
		};
	},

	shareWindow: function(urlProduct, urlMediaProduct){
		var facebook = "https://www.facebook.com/sharer/sharer.php?u=" + urlProduct,
		twitter = "https://twitter.com/home?status=" + urlProduct,
		pinterest = "https://pinterest.com/pin/create/button/?url=" + urlProduct + "&media=" + urlMediaProduct,
		gplus = "https://plus.google.com/share?url=" + urlProduct;

		$(".shareProduct .facebook").attr("href", facebook);
		$(".shareProduct .twitter").attr("href", twitter);
		$(".shareProduct .pinterest").attr("href", pinterest);
		$(".shareProduct .gplus").attr("href", gplus);

		$(".shareProduct a").on("click", function(){
        	newwindow=window.open($(this).attr('href'),'','height=400,width=400');
        	if (window.focus) {newwindow.focus()}
        	return false;	
		});
	},

	imageFull: function($element){
		$element.each(function() {
           var bImg = $(this).find("img").attr("src");
           $(this).find("a").attr("style","background-image:url("+bImg+");background-repeat: no-repeat; background-position: center;");
           $(this).find("img").remove();
        });
	},

	verifyWidth: function () {
		if ($(window).width() <= 1024) {
			$('body').addClass('isMobile');
			$('body').removeClass('notMobile');
			return true
		} else {
			$('body').removeClass('isMobile');
			$('body').addClass('notMobile');
			return false
		}
	},

	wrapText: function(string, strLength, complement) {
		if(string.substring().length > strLength) {
			return string.substring(0, strLength) + complement;
		} else {
			return string;
		}
    },
}

var global = {
	init: function () {
    	global.menu();
		global.menuOver();
		global.menuToggle();
		global.toggleSearch();
		global.footerMenuToggle();
		
		$(window).load(function () {
            global.userLogged();    
		})
		
		$(document).ajaxStop(function(){
            global.autocompleteClone();
        })
	},

    menuAberto: function(){
        if(vtxctx.departmentName != undefined){
            if(location.pathname.split('/')[1] == 'women'){
                $('.navigation .menu-departamento>h3').removeClass('active');
                $('.navigation .menu-departamento .submenu').hide();

                $('.navigation .menu-departamento>h3.Women').addClass('active');
                $('.navigation .menu-departamento .submenu#Women').fadeIn();
            }else if(location.pathname.split('/')[1] == 'men'){
                $('.navigation .menu-departamento>h3').removeClass('active');
                $('.navigation .menu-departamento .submenu').hide();

                $('.navigation .menu-departamento>h3.Men').addClass('active');
                $('.navigation .menu-departamento .submenu#Men').fadeIn();
            }else if(location.pathname.split('/')[1] == 'kids'){
                $('.navigation .menu-departamento>h3').removeClass('active');
                $('.navigation .menu-departamento .submenu').hide();

                $('.navigation .menu-departamento>h3.Kids').addClass('active');
                $('.navigation .menu-departamento .submenu#Kids').fadeIn();
            }
        }else{
            $('.navigation .menu-departamento>h3.Women').addClass('active');
            $('.navigation .menu-departamento .submenu#Women').fadeIn();
        }/*fim menu aberto*/
    },

    menu: function(){
		$(".pageNav .menu-departamento h3").each(function() {
            var depClass = $(this).removeClass('even').attr('class');
            var that = $(this);
        });
        $(".pageNav .menu-departamento h3").each(function (index) {
            $(this).css('display','inline-block');
        })
    },
    
    menuOver: function() {
        var menuOutObject;
        var menuOutTimer;
        $('.pageNav .menu-departamento h3.hasSubmenu').hover(

            function() {

                $('.navigation .menu-departamento>h3').removeClass('active');

                if (!fns.verifyWidth()) {
					menuOutObject = $(this).next('.submenu');
					$(this).addClass('active');
                    
                    if (!menuOutObject.is(':visible')) {
                        hideMenuSubItems($('.pageNav .menu-departamento .submenu:visible'));
                    }
                    clearTimeout(menuOutTimer);
                    menuOutObject.fadeIn(300);
                    //$('.navOverlay').addClass('active').fadeIn(300);
                }
                
            },
            
            function() {
				if (!fns.verifyWidth()) {
					$(this).removeClass('active');
                	menuOutTimer = setTimeout(function() {
						hideMenuSubItems(menuOutObject);
                        //$('.navOverlay').removeClass('active').fadeOut(300);
					}, 10);
				}
            }
        );
        
        $('.pageNav .menu-departamento .submenu').hover(
            function() {
                if (!fns.verifyWidth()) {
                    menuOutObject = $(this);
                    clearTimeout(menuOutTimer);
                }
            },
            function() {
                menuOutTimer = setTimeout(function() {
                    if (!fns.verifyWidth()) {
                        hideMenuSubItems(menuOutObject);
                        //$('.navOverlay').removeClass('active').fadeOut(300);
                    }
                }, 10);
            }
        );
        
        if (!fns.verifyWidth()) {
            $(".pageNav .menu-departamento .submenu").mouseover(function(){
				$(this).prev('h3').addClass('active');
            });
            $(".pageNav .menu-departamento .submenu").mouseout(function(){
				$(this).prev('h3').removeClass('active');
            });
        };
        
        function hideMenuSubItems(o) {
            o.stop(true,true).fadeOut(300);

            setTimeout(function(){
                if($('.navigation .menu-departamento>h3.active').size() < 1){
                    global.menuAberto();
                }
            },200);
        };
    },
    
    menuToggle: function () {
        // Open Menu
        $('.menu-mobile-toggle').on('click', function (e) {
            if (fns.verifyWidth()) {
                e.preventDefault();
                $('.pageNav').addClass('open');
                $('body').addClass('menuOpen');
                if ($('.navOverlay').css("display") == "none") {
                    $('.navOverlay').fadeIn(300);
                }
                //$('.navOverlay').addClass('active');
            }
        })
        
        //Close Menu
        $('.navHead i, .navOverlay').on('click', function (e) {
            if (fns.verifyWidth()) {
                e.preventDefault();
                $('.pageNav').removeClass('open');
                $('body').removeClass('menuOpen');
                if ($('.navOverlay').css("display") == "block") {
                    $('.navOverlay').fadeOut(300);
                }
                //$('.navOverlay').removeClass('active');
            }
        });
        
        
        //Open Submenu
        $('.menu-departamento h3').on('click', function (e) {
            if (fns.verifyWidth()) {
                if($(this).hasClass('hasSubmenu')){
                    e.preventDefault();            		
                }
                var that = $(this);
                var depLink = that;
                var submenu = depLink.next();
                
                if (!submenu.hasClass('submenuOpen')) {
                    $('.submenu').removeClass('submenuOpen').stop(true,true).hide(300);
                    $('.menu-departamento .ico-expand').removeClass('active');
                    $('.menu-departamento h3').removeClass('active');
                    that.find('.ico-expand').addClass('active');
                    depLink.addClass('active');
                    submenu.addClass('submenuOpen').stop(true,true).show(300);
                } else {
                    that.find('.ico-expand').removeClass('active');
                    depLink.removeClass('active');
                    submenu.removeClass('submenuOpen').stop(true,true).hide(300);
                }
            }
        })
    },
	
	userLogged: function(){
        vtexjs.checkout.getOrderForm().done(function(data){
            var userName;
            if (data.loggedIn){ // Se logado
                if(data.clientProfileData.firstName != null) {
                    userName = data.clientProfileData.firstName;
                } else {
                    userName = data.clientProfileData.email;
                }
				$('.customerService .login a').text('Logout').attr("href", "/no-cache/user/logout");
			} else { // se NÃO logado
				$('.customerService .login a').text('Login').attr("href", "/sistema/401");
            }
        });
	},
	
	autocompleteClone: function() {
        $('.ui-autocomplete').appendTo('.searchBox');
        //$('.ui-autocomplete').unbind();
        
        $('.ui-autocomplete li').each(function() {
            if($(this).find('img').length > 0) {
                
                $(this).find('a').wrapInner('<span/>')
                $(this).find('a').prepend($(this).find('img'));
                
                $(this).addClass('hasImage');
                var imgSrc = $(this).find('img').attr('src').replace('25-25','265-265');
                $(this).find('img').attr('src' , imgSrc).prop('width','265').prop('height','265');
            }
        });
        
        $('.hasImage').wrapAll('<ul class="product-found"></ul>')
	},
	
	toggleSearch: function () {
        $('.toggleSearch .fas').on('click', function(e) {
			e.preventDefault();
			$('.toggleSearch').stop(true,true).toggleClass('active');
        })
		
    },

    footerMenuToggle: function () {
        $('.toggle h3').on('click', function (e) {
            e.preventDefault();
            var that = $(this);
            var itemContent = that.parent().find('.itemContent');
            
			if ($(window).width() <= 720) {
                if (!itemContent.hasClass('active')) {
                    $('.toggle h3').parent().find('.itemContent').stop(true,true).hide(300).removeClass('active');
                    itemContent.addClass('active').stop(true,true).show(300);
                    $('.toggle h3').removeClass('active');
                    that.addClass('active');
				} else {
					that.removeClass('active');
					itemContent.stop(true,true).hide(300).removeClass('active');
				}
			}
        })
    },
}

var slider = {
	fullSlider: function (element, dots, arrows, backgroundImage) { // banner tv fullscreen        
        if (backgroundImage) {
            fns.imageFull(element.find('.box-banner'));
        };

        element.slick({
            dots: dots,
            arrows: arrows,
            pauseOnHover: false,
            autoplay: true,
            autoplaySpeed: 6000            
        });
    },

    fullSliderTabs: function (dots, arrows) { // banner tv fullscreen com abas
        var arrayDots = [];
        $(".fullGallery .box-banner").each(function() {
           var bImg = $(this).find("img").attr("src");
           $(this).find("a").attr("style","background:url("+bImg+") no-repeat center;");
           if(!$(this).hasClass('slick-cloned')) {
                var dotText = $(this).find('img').attr('alt');
                arrayDots.push(dotText)
            }
           $(this).find("img").remove();
        });
        
        $(".mainGallery").slick({
            dots: true,
            arrows: false,
            pauseOnHover: false,
            autoplay: false,
            autoplaySpeed: 4000
        });

        $('.mainGallery .slick-dots li').each(function (i) {
            $(this).find('button').text(arrayDots[i]);
        });
    },

	singleSlider: function (dots, arrows) {
        $(".mainGallery").slick({
			dots: true,
			arrows: false,
			pauseOnHover: false,
			autoplay: true,
  			autoplaySpeed: 4000
		});
	},

	shelfSlider: function (dots, arrows, slidesToShow, slidesToScroll) { // carrossel de produtos
        $(".shelfCarousel").each(function() {
            $(this).find("ul").slick({
                dots: dots,
                arrows: arrows,
                slidesToShow: slidesToShow,
                slidesToScroll: slidesToScroll,
                pauseOnHover: false,
                autoplay: false,
                autoplaySpeed: 4000
            });
        });
    },

	multipleSlider: function (slidesToShow, slidesToScroll, vertical) {
		$(".carouselGallery").slick({
		    infinite: true,
		    slidesToShow: slidesToShow,
			slidesToScroll: slidesToScroll,
		    speed: 500,
		    vertical: vertical
		});
	},
}

var home = {
    init: function() {
        home.instafeed();
        home.newsletterRegister();
    },

    instafeed: function() {
		var	instagramClientId = '22951031',
        instagramDeveloperToken = '22951031.603c206.a85def2a4ea648eca67e0c3988431e50';
        $.getJSON('https://api.instagram.com/v1/users/' + instagramClientId + '/media/recent/?access_token=' + instagramDeveloperToken + '&callback=?', function(data) {
            for (i = 0; i < 6; i++) {
                var imageUrl = data.data[i].images.low_resolution.url;
                var item = '<div class="instaItem" style="background-image: url('+imageUrl+')"><a href="' + data.data[i].link + '" target="_blank"></a></div>';
                $('#instagram').append(item);
            }
        }).done(function(){
            $('#instagram').show();
        });
    },

    newsletterRegister: function () {
        $('.formOptin button').on('click', function(e) {
            e.preventDefault();
            
            var _that = $(this);
            var btnVal = _that.val();
            var parent = _that.parents('fieldset');
            
            var emailUser = parent.find('#ClientEmail').val();
            var cupon = "Thank you for singning up";
            
            function validateEmail(email) {
                var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
                return re.test(email);
            }
            
            _that.prop("disabled",true);
                            
            if(validateEmail(emailUser)) {
                var jsonData = {
                    "email": emailUser
                };
                
                // Requisição Ajax
                vtexjs.checkout.getOrderForm().then(function(orderForm) {
                    $.ajax({
                        headers: {
                            "Accept": "application/vnd.vtex.ds.v10+json",
                            "Content-Type": "application/json",
                            "REST-Range": "resources=0-1"
                        },
                        type: 'GET',
                        url: "//api.vtexcrm.com.br/shopcouture/dataentities/NR/search?_fields=id,email&_where=email=" + emailUser
                        
                    }).done(function(clientInfo) {
                        if(clientInfo.length == 0) { // Não existe email                           
                            $.ajax({
                                url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/NR/documents/',
                                dataType: 'json',
                                type: 'PATCH',
                                crossDomain: true,
                                data: JSON.stringify(jsonData),
                                headers: {
                                    'Accept': 'application/vnd.vtex.ds.v10+json',
                                    'Content-Type': 'application/json; charset=utf-8'
                                },
                                success:function(emailUser){                                        
                                    $(parent).find('.campos').hide();
                                    $(parent).find('.success').html(cupon);
                                    $(parent).find('.success').show();
                                    _that.val(btnVal).prop("disabled",false);
                                }
                            });
                        } else {
                            $(parent).find('.duplicate').show();
                            $(parent).find('.campos').hide();
                            _that.val(btnVal).prop("disabled",false);
                        }
                    })
                });
            } else {
                $(parent).find('.server').show();
                $(parent).find('.campos').hide();
                _that.val(btnVal).prop("disabled",false);
            }
            
        });
        $('.tryAgain').on('click', function(e) {
            e.preventDefault();
            var parent = $(this).parents('fieldset');
            $(parent).find('.erro').hide();
            $(parent).find('.campos').show();
        });
    }
}

var catalog = {
	smartResearch: function(){
		$(".navSidebar input[type='checkbox']").vtexSmartResearch();	
	},

	buscaVazia: function () {
		var pagina_busca = $('.search-result'),
	        busca_vazia = $('.busca-vazio');

		if (pagina_busca[0]) {
	        var url = window.location.toString().split('/'),
	            split_ft = url[3].split('?'),
	            termo = split_ft[0];
	        
	        if (busca_vazia[0]) {
	            pagina_busca.addClass('no-result');
	            $(".busca-vazio").append("<ul><li>Tente palavras menos específicas.</li><li>Digite pelo menos quatro caracteres no campo de busca.</li><li>Use os menus do site para navegar pelos departamentos de produtos</ul>");
				$(".navSidebar").remove();
	        } else {
	            var valorResult = $('.resultado-busca-termo:eq(0)').find('.value').text();
	            pagina_busca.addClass('resultado-encontrado');
	        };
	    }; 
	},

	searchWord: function () {
		var word = $(".resultado-busca-termo:eq(0)").find("strong").text();
		$(".navTopbar .titulo-sessao").html('<em>Você buscou por:</em>"' + word + '"');
	},

	init: function  () {
	}
}

var product = {
    share: function(){
		var urlProduct = window.location.href;
		var urlMediaProduct = $("#image img").attr("src");
		
		fns.shareWindow(urlProduct, urlMediaProduct);
	},

	superZoom: function (width, height) {
		window.LoadZoom = function (pi) {
			if($(".image-zoom").length<=0) return false;			
			var optionsZoom = {
				zoomWidth: width,
				zoomHeight: height,
				preloadText: ''
			};			
	    	$(".image-zoom").jqzoom(optionsZoom);
		}
	    LoadZoom(0);
	},

	init: function(){
		product.share();
		product.superZoom(300,300);
	}
}

var institutional = {
	linkSidebar: function (){
        $(".institutionalLinks li a").each(function(){
            var link = $(this).attr('href');
            var url = window.location.pathname;
            if(link == url){
                $(this).addClass("current");
            }
        });
    },

    init: function(){
    	institutional.linkSidebar();
    }
}

$(document).ready(function () {
    (function(d){if("function"!==typeof d.qdAjax){var a={};d.qdAjaxQueue=a;150>parseInt((d.fn.jquery.replace(/[^0-9]+/g,"")+"000").slice(0,3),10)&&console&&"function"==typeof console.error&&console.error();d.qdAjax=function(f){try{var b=d.extend({},{url:"",type:"GET",data:"",success:function(){},error:function(){},complete:function(){},clearQueueDelay:5},f),e;e="object"===typeof b.data?JSON.stringify(b.data):b.data.toString();var c=encodeURIComponent(b.url+"|"+b.type+"|"+e);a[c]=a[c]||{};"undefined"==
    typeof a[c].jqXHR?a[c].jqXHR=d.ajax(b):(a[c].jqXHR.done(b.success),a[c].jqXHR.fail(b.error),a[c].jqXHR.always(b.complete));a[c].jqXHR.always(function(){isNaN(parseInt(b.clearQueueDelay))||setTimeout(function(){a[c].jqXHR=void 0},b.clearQueueDelay)});return a[c].jqXHR}catch(g){"undefined"!==typeof console&&"function"===typeof console.error&&console.error("Problemas no $.qdAjax :( . Detalhes: "+g.message)}};d.qdAjax.version="4.0"}})(jQuery);

    global.menuAberto();

    $(".helperComplement").remove();

    function busca(){
        $('.ui-autocomplete').appendTo('header .customerService .searchBox');
    }
    function autocompleteClone(){
        $('.ui-autocomplete').appendTo('header .customerService .searchBox');
        
        $('.ui-autocomplete li').each(function() {
            if($(this).find('img').length > 0) {
                
                $(this).find('a').wrapInner('<span/>')
                $(this).find('a').prepend($(this).find('img'));
                
                $(this).addClass('hasImage');
                var imgSrc = $(this).find('img').attr('src').replace('25-25','40-40');
                $(this).find('img').attr('src' , imgSrc).prop('width','40').prop('height','60');
            }
        });
        $('.hasImage').wrapAll('<ul class="product-found"></ul>');
    }
    busca();
    $(document).ajaxStop(function(){
        autocompleteClone();
    });

	global.init();

    if($('body').hasClass("internasComum")) {
        /***Pesquisa Inteligente ***/
        "function"!=typeof String.prototype.replaceSpecialChars&&(String.prototype.replaceSpecialChars=function(){var a={"ç":"c","æ":"ae","œ":"oe","á":"a","é":"e","í":"i","ó":"o","ú":"u","à":"a","è":"e","ì":"i","ò":"o","ù":"u","ä":"a","ë":"e","ï":"i","ö":"o","ü":"u","ÿ":"y","â":"a","ê":"e","î":"i","ô":"o","û":"u","å":"a","ã":"a","ø":"o","õ":"o",u:"u","Á":"A","É":"E","Í":"I","Ó":"O","Ú":"U","Ê":"E","Ô":"O","Ü":"U","Ã":"A","Õ":"O","À":"A","Ç":"C"};return this.replace(/[\u00e0-\u00fa]/g,function(b){return"undefined"!=typeof a[b]?a[b]:b})}),"function"!=typeof String.prototype.trim&&(String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")}),jQuery.fn.vtexSmartResearch=function(a){$this=jQuery(this);var b=function(a,b){"object"==typeof console&&console.log("[Smart Research - "+(b||"Erro")+"] "+a)},c={pageLimit:null,loadContent:".prateleira[id^=ResultItems]",shelfClass:".prateleira",filtersMenu:".search-multiple-navigator",linksMenu:".search-single-navigator",menuDepartament:".navigation .menu-departamento",mergeMenu:!0,insertMenuAfter:".search-multiple-navigator h3:first",emptySearchElem:jQuery('<div class="vtexsr-emptySearch"></div>'),elemLoading:'<div id="scrollLoading">Loading...</div>',returnTopText:'<span class="text">voltar ao</span><span class="text2">TOPO</span>',emptySearchMsg:"<h3>Esta combinaÃ§Ã£o de filtros nÃ£o retornou nenhum resultado!</h3>",filterErrorMsg:"Houve um erro ao tentar filtrar a pÃ¡gina!",searchUrl:null,usePopup:!1,showLinks:!0,popupAutoCloseSeconds:3,filterScrollTop:function(a){return a.top-20},callback:function(){},getShelfHeight:function(a){return a.scrollTop()+a.height()},shelfCallback:function(){console.log("shelfCallback")},ajaxCallback:function(){console.log("ajaxCallback")},emptySearchCallback:function(){console.log("emptySearchCallback")},authorizeScroll:function(){return console.log("authorizeScroll"),!0},authorizeUpdate:function(){return console.log("authorizeUpdate"),!0},labelCallback:function(a){console.log("labelCallback")}},d=jQuery.extend(c,a),f=("object"==typeof console,jQuery("")),g=jQuery(d.elemLoading),h=2,i=!0,j=jQuery(window),l=(jQuery(document),jQuery("html,body")),m=jQuery("body"),n="",o="",p="",q=!1,r=jQuery(d.loadContent),s=jQuery(d.filtersMenu),t={requests:0,filters:0,isEmpty:!1},u={},v={getUrl:function(a){var b=a||!1;return b?n.replace(/PageNumber=[0-9]*/,"PageNumber="+h):(p+o).replace(/PageNumber=[0-9]*/,"PageNumber="+B)},getSearchUrl:function(){var a,c,d;return jQuery("script:not([src])").each(function(){if(c=jQuery(this)[0].innerHTML,d=/\/buscapagina\?.+&PageNumber=/i,c.search(/\/buscapagina\?/i)>-1)return a=d.exec(c),!1}),"undefined"!=typeof a&&"undefined"!=typeof a[0]?a[0]:(b("NÃ£o foi possÃ­vel localizar a url de busca da pÃ¡gina.\n Tente adicionar o .js ao final da pÃ¡gina. \n[MÃ©todo: getSearchUrl]"),"")},scrollToTop:function(){var a=m.find("#returnToTop");a.length<1&&(a=jQuery('<div id="returnToTop"><a href="#">'+d.returnTopText+'<span class="arrowToTop"></span></a></div>'),m.append(a));var b=j.height();j.bind("resize",function(){b=j.height()}),j.bind("scroll",function(){j.scrollTop()>b?a.stop(!0).fadeTo(300,1,function(){a.show()}):a.stop(!0).fadeTo(300,0,function(){a.hide()})}),a.bind("click",function(){return l.animate({scrollTop:0},"slow"),!1})},infinitScroll:function(){var a,b,c,e;a=m.find(".pager:first").attr("id"),e=(a||"").split("_").pop(),b=null!==d.pageLimit?d.pageLimit:window["pagecount_"+e],c=!0,"undefined"==typeof b&&(b=99999999),b>1&&r.append("<div class='btn-load-more confira-todos-produtos'><span>see more products</span></div>"),r.live("click",".btn-load-more",function(){setTimeout(function(){descontoPrateleira();},2000);var a=jQuery(this);if(!q&&h<=b&&i&&d.authorizeScroll(t)){if(a.scrollTop()+a.height()>=d.getShelfHeight(r)&&c){var e=r.find(d.shelfClass).filter(":last");e.after(g),c=!1,window.pftxcurrentSearchUrl=v.getUrl(!0),D=jQuery.ajax({url:v.getUrl(!0),success:function(a){a.trim().length<1?(i=!1,console.log("Não existem mais resultados a partir da página: "+(h-1),"Aviso"),r.find(".btn-load-more").fadeOut("fast")):($(".btn-load-more").length>0?(console.log("maior que 0"),h>b&&r.find(".btn-load-more").fadeOut("fast")):console.log("menor que 0"),e.after(a)),c=!0,g.remove(),t.requests++,d.ajaxCallback(t)}}),h++}}else r.find(".btn-load-more").fadeOut("fast")})}};if(n=p=null!==d.searchUrl?d.searchUrl:v.getSearchUrl(),$this.length<1)return b("Nenhuma opção de filtro encontrada","Aviso"),d.showLinks&&jQuery(d.linksMenu).css("visibility","visible").show(),v.infinitScroll(),v.scrollToTop(),$this;if(r.length<1)return b("Elemento para destino da requisição não foi encontrado \n ("+r.selector+")"),!1;s.length<1&&b("O menu de filtros não foi encontrado \n ("+s.selector+")");var x=(document.location.href,jQuery(d.linksMenu)),y=jQuery('<div class="vtexSr-overlay"></div>'),z=jQuery(d.menuDepartament),A=r.offset(),B=1,C=null,D=null;d.emptySearchElem.append(d.emptySearchMsg),r.before(y);var E={exec:function(){E.setFilterMenu(),E.fieldsetFormat(),$this.each(function(){var a=jQuery(this),b=a.parent();a.is(":checked")&&(o+="&"+(a.attr("rel")||""),b.addClass("sr_selected")),E.adjustText(a),b.append('<span class="sr_box"></span><span class="sr_box2"></span>'),$(".menu-departamento").on("change",a,function(){E.inputAction(),a.is(":checked")?E.addFilter(a):E.removeFilter(a),t.filters=$this.filter(":checked").length})}),""!==o&&E.addFilter(f)},mergeMenu:function(){if(!d.mergeMenu)return!1;var a=z;a.insertAfter(d.insertMenuAfter),E.departamentMenuFormat(a)},mergeMenuList:function(){var a=0;s.find("h3,h4").each(function(){var b=x.find("h3,h4").eq(a).next("ul");b.insertAfter(jQuery(this)),E.departamentMenuFormat(b),a++})},departamentMenuFormat:function(a){a.find("a").each(function(){var a=jQuery(this);a.text(E.removeCounter(a.text()))})},fieldsetFormat:function(){u.fieldsetCount=0,u.tmpCurrentLabel={},s.find("fieldset").each(function(){var a=jQuery(this),b=a.find("label"),c="filtro_"+(a.find("h5:first").text()||"").toLowerCase().replaceSpecialChars().replace(/\s/g,"-");return u[c]={},b.length<1?void a.hide():(a.addClass(c),b.each(function(b){var e=jQuery(this),f=e.find("input").val()||"",g="sr_"+f.toLowerCase().replaceSpecialChars().replace(/\s/g,"-");u.tmpCurrentLabel={fieldsetParent:[a,c],elem:e},u[c][b.toString()]={className:g,title:f},e.addClass(g).attr({title:f,index:b}),d.labelCallback(u)}),void u.fieldsetCount++)})},inputAction:function(){null!==D&&D.abort(),null!==C&&C.abort(),h=2,i=!0},addFilter:function(a){o+="&"+(a.attr("rel")||""),y.fadeTo(300,.6),n=v.getUrl(),window.pftxcurrentSearchUrl=n,C=jQuery.ajax({url:n,success:E.filterAjaxSuccess,error:E.filterAjaxError}),a.parent().addClass("sr_selected")},removeFilter:function(a){var b=a.attr("rel")||"";y.fadeTo(300,.6),""!==b&&(o=o.replace("&"+b,"")),n=v.getUrl(),window.pftxcurrentSearchUrl=n,C=jQuery.ajax({url:n,success:E.filterAjaxSuccess,error:E.filterAjaxError}),a.parent().removeClass("sr_selected")},filterAjaxSuccess:function(a){var b=jQuery(a);y.fadeTo(300,0,function(){jQuery(this).hide()}),E.updateContent(b),t.requests++,d.ajaxCallback(t),l.animate({scrollTop:d.filterScrollTop(A||{top:0,left:0})},600)},filterAjaxError:function(){y.fadeTo(300,0,function(){jQuery(this).hide()}),console.log(d.filterErrorMsg),b("Houve um erro ao tentar fazer a requisição da página com filtros.")},updateContent:function(a){if(q=!0,!d.authorizeUpdate(t))return!1;var b=a.filter(d.shelfClass),c=r.find(d.shelfClass);(c.length>0?c:d.emptySearchElem).slideUp(600,function(){jQuery(this).remove(),d.usePopup?m.find(".boxPopUp2").vtexPopUp2():d.emptySearchElem.remove(),b.length>0?(b.hide(),$(".btn-load-more").length>0?r.find(".btn-load-more").fadeIn("fast").before(b):r.append(b),d.shelfCallback(),b.slideDown(600,function(){q=!1}),t.isEmpty=!1):(t.isEmpty=!0,d.usePopup?d.emptySearchElem.addClass("freeContent autoClose ac_"+d.popupAutoCloseSeconds).vtexPopUp2().stop(!0).show():(r.append(d.emptySearchElem),console.log("mlezao - 492"),d.emptySearchElem.show().css("height","auto").fadeTo(300,.2,function(){d.emptySearchElem.fadeTo(300,1)})),d.emptySearchCallback(t))})},adjustText:function(a){var b=a.parent(),c=b.text();qtt="",c=E.removeCounter(c),b.text(c).prepend(a)},removeCounter:function(a){return a.replace(/\([0-9]+\)/gi,function(a){return qtt=a.replace(/\(|\)/,""),""})},setFilterMenu:function(){s.length>0&&(x.hide(),s.show())}};m.hasClass("departamento")?E.mergeMenu():(m.hasClass("categoria")||m.hasClass("resultado-busca"))&&E.mergeMenuList(),E.exec(),v.infinitScroll(),v.scrollToTop(),d.callback(),s.css("visibility","visible")};var smartResearch;

        var thisNumeroEncontrado = $('.resultado-busca-numero span.value').html();
        var thisTermoDigitado = $('.resultado-busca-termo strong.value').html();

        if($('body').hasClass('resultado-busca')){
            $('.bread-crumb ul').append('<li class="last"><strong><a href="/">RESULTS SEARCH</a></strong></li>')
            $('.wrapperRight').prepend("<div class='resultadoDaBusca'>We've found " + thisNumeroEncontrado + " results for the word '" + thisTermoDigitado + "'</div>");
        }else{
            //$('.wrapperRight').prepend("We've found " + thisNumeroEncontrado + " results for the word '" + thisTermoDigitado + "'");
        }

        $('.wrapperRight .main .sub .orderBy').append('<div class="selectFake"><span>Selecione</span></div>');

        $('.wrapperRight .main .sub .resultado-busca-filtro').prepend('<button class="filtrar"><div><span></span><span></span><span></span></div>Filtros</button>');


        if($('.sidebar .search-single-navigator h4').size() > 0){
            $('.sidebar .search-multiple-navigator h4').remove();
            $('.sidebar .search-multiple-navigator h3 a').html('CATEGORIES');
            $('.sidebar .search-multiple-navigator h3').wrap('<fieldset class="wrapCats comSub"/>');
            $('.sidebar .search-multiple-navigator fieldset.wrapCats').append('<div class="cat"></div>');
            $('.sidebar .search-single-navigator h4').each(function(){
                $(this).find('a').html($(this).find('a').attr('title'));
                var thisH4 = $(this).html();
                $('.sidebar .search-multiple-navigator fieldset.wrapCats .cat').append('<label>'+thisH4+'</label>');
            });
            setTimeout(function(){
                $('.sidebar .search-multiple-navigator fieldset.wrapCats').show();
            },1000);
        }else{
            $('.sidebar .search-multiple-navigator h4').remove();
            $('.sidebar .search-multiple-navigator h3').remove();
        }

        $(".sidebar input[type='checkbox']").vtexSmartResearch({
            pageLimit:null,
            loadContent:".prateleira[id^=ResultItems]",
            shelfClass:".prateleira",
            filtersMenu:".search-multiple-navigator",
            linksMenu:".search-single-navigator",
            menuDepartament:".sidebar .navigation .menu-departamento",
            mergeMenu:true, 
            insertMenuAfter:".search-multiple-navigator h3:first",
            emptySearchElem:jQuery('<div class="vtexsr-emptySearch"></div>'),
            elemLoading:'<div id="scrollLoading">Loading... </div>',
            returnTopText:'<span class="text">voltar ao</span><span class="text2">TOPO</span>',
            emptySearchMsg:'<h3>This filter combination did not return any results!</h3>',
            filterErrorMsg:"There was an error while trying to filter the page!",
            showLinks:true,
            popupAutoCloseSeconds:3, 
            filterScrollTop:function(shelfOffset){
                //return (shelfOffset.top-20);
            },
            callback:function(){},
            shelfCallback:function(){},
            getShelfHeight:function(container){
                return (container.scrollTop()+container.height());
            }
        });

        $('.sidebar fieldset.filtro_marca label').live('click',function(){
            if(!$(this).hasClass('sr_selected')){
                var sizeActive = $('.sidebar fieldset.filtro_marca label.sr_selected').size();
                if(sizeActive > 0){
                    alert('Select only one designer!');
                    return false;
                }
            }
        });

        $('.sidebar .search-multiple-navigator fieldset.wrapCats .cat label').prepend('<input type="checkbox"/>');

        setTimeout(function(){
            $('.sidebar').fadeIn();
        },1000);

        $('button.filtrar').live('click', function(){
            $('.sidebar').animate({
               left: "0"
            },400);
        });

        $('.sidebar .overlay button, .sidebar .top button.close').live('click', function(){
            $('.sidebar').animate({
               left: "-100%"
            },200);
        });

        if($('body').hasClass('brand')){
            var termoResultados = $('meta[name="Abstract"]').attr('content');
            $('.titlePag h2.titulo-sessao').html('DESIGNER "'+termoResultados+'"');
            $('.titlePag h2.titulo-sessao').fadeIn();
        }/*FIM BRAND*/

        if($('body').hasClass('resultado-busca')){
            if($('.resultado-busca-numero').length){
                var numResultados = $('.resultado-busca-numero:eq(0) .value').text();
                var termoResultados = $('.resultado-busca-termo:eq(0) .value').text();

                $('.titlePag h2.titulo-sessao').html("We've found "+numResultados+" results for the word '"+termoResultados+"'");
                $('.titlePag h2.titulo-sessao').fadeIn();

            }else if($('body').hasClass('busca-vazia')){
                $('#wrapperPrateleira .prateleira > ul').slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    dots: true,
                    arrows: false,
                    responsive: [
                        {
                          breakpoint: 680,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                          }
                        },
                        {
                          breakpoint: 420,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                          }
                        }
                    ]
                });
                $('#wrapperPrateleira').fadeIn();

                var termoResultados = $('meta[name="Abstract"]').attr('content');
                $('.resultadoDaBusca span').html('No results were found for "'+termoResultados+'"');
                $('.resultadoDaBusca').fadeIn();
            }

            if($('.navegacaoFiltros .search-multiple-navigator fieldset.desktop').length && $('.navegacaoFiltros .search-multiple-navigator fieldset.desktop').length){
                $('.navegacaoFiltros').fadeIn();
            }

        }/*FIM RESULTADO DE BUSCA*/

        $('.titlePag h2.titulo-sessao').fadeIn();

        $('body.internasComum .sidebar fieldset.sortBy label').live('click',function(){
            var thisLabelClick = $(this).attr('sort');

            window.location = window.location.origin+window.location.pathname+"?"+thisLabelClick;
        });

        var thisSort = window.location.search;
        var thisSort = thisSort.replace('?','');
        $('body.internasComum .sidebar fieldset.sortBy label[sort="'+thisSort+'"]').addClass('ativo');

    };

	if ($('body').hasClass("home")) {
        home.init();

        $(".mainGallery").slick({
            dots: true,
            arrows: false,
            pauseOnHover: false,
            autoplay: true,
            autoplaySpeed: 4000
        });
        
		$(window).load(function () {
            //slider.fullSlider($('.fullGallery'), false, false, false);            
        }); 

        $('.featuredProducts .prateleira > ul').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: true,
            arrows: true,
            responsive: [
                {
                  breakpoint: 680,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 420,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        });
        $('.featuredProducts').fadeIn();
	}

	if ($('body').hasClass("produto")) {

        function ajaxPag(){
            if($('section#area-a .right .wrapper.botao .notifyme').is(':visible')){
                $('.right .wrapper.botao .notifyme form fieldset.notifyme-form input[type="button"]').val('Warn me');
                $('').hide();
            }else{
                setTimeout(function(){
                    $('').fadeIn();
                },1000);/*FIM FRETE*/

                var thisNome = $('section#area-a .right .wrapper.nome .productName').text();
                var thisPreco = $('section#area-a .right .wrapper.preco .valor-por strong').text();
            }
        }
        ajaxPag();
    
        /*thumbsS MOBILE*/
        function thumbsMobile(){
            //console.log('thumbsMobile');
            var onResizeWidth = $(window).width();  
            if(onResizeWidth <= 920){
                $('section#area-a .left .apresentacao #show ul.thumbs li img').each(function(){
                    var thisImg = $(this).attr('src');
                    var newThisImg = thisImg.replace('110-110', '600-600');
                    $(this).attr('src', newThisImg);
                });
                $('section#area-a .left .apresentacao #show ul.thumbs').slick({
                    dots: true,
                    arrows: false,
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    speed: 600,
                    slidesToShow: 1,
                    slidesToScroll: 1               
                });
                $('section#area-a .left .apresentacao').fadeIn();   
            }else{
                $('section#area-a .left .apresentacao #show ul.thumbs li img').each(function(){
                    var thisImg = $(this).attr('src');
                    // var newThisImg = thisImg.replace('110-110', '600-600');
                    // $(this).attr('src', newThisImg);
                });
                if($('body.produto .tpl-center section#area-a .left .apresentacao ul.thumbs li').size() > 3){
                    $('body.produto .tpl-center section#area-a .left .apresentacao ul.thumbs').css('margin-top', '20px');
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick({
                        dots: false,
                        arrows: true,
                        infinite: true,
                        speed: 600,
                        vertical: true,
                        verticalSwiping: true,
                        pauseOnHover: true,
                        slidesToShow: 3,
                        slidesToScroll: 2               
                    });
                }
                setTimeout(function(){
                    $('section#area-a .left .apresentacao ul.thumbs li[data-slick-index="0"] a').click();
                },500);
                setTimeout(function(){
                    $('section#area-a .left .apresentacao').fadeIn();
                },700);
            }
            window.addEventListener('resize', function(){
                if (window.innerWidth <= 920) {
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick('unslick');
                    $('section#area-a .left .apresentacao #show ul.thumbs li img').each(function(){
                        var thisImg = $(this).attr('src');
                        var newThisImg = thisImg.replace('110-110', '600-600');
                        $(this).attr('src', newThisImg);
                    });
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick({
                        dots: true,
                        arrows: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        speed: 600,
                        slidesToShow: 1,
                        slidesToScroll: 1               
                    });
                    $('section#area-a .left .apresentacao').fadeIn();
                }else{
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick('unslick');
                    $('section#area-a .left .apresentacao #show ul.thumbs li img').each(function(){
                        var thisImg = $(this).attr('src');
                        var newThisImg = thisImg.replace('110-110', '600-600');
                        $(this).attr('src', newThisImg);
                    });
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick('unslick');
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick({
                        dots: false,
                        arrows: true,
                        infinite: true,
                        speed: 500,
                        vertical: true,
                        verticalSwiping: true,
                        pauseOnHover: true,
                        slidesToShow: 3,
                        slidesToScroll: 2               
                    });
                    $('section#area-a .left .apresentacao').fadeIn();
                }
            });
        }
        thumbsMobile();/*FIM THUMBS MOBILE*/

        $('section#area-a .right .wrapper.sku ul li.select input[type="radio"]').on('click', function(){
            setTimeout(function() {
                if($('section#area-a .left .apresentacao #show ul.thumbs').hasClass('slick-slider')){
                    $('section#area-a .left .apresentacao #show ul.thumbs').html('');
                    $('section#area-a .left .apresentacao #show ul.thumbs').slick('unslick');
                }
                thumbsMobile();
            }, 10);
        });/*FIM THUMBS MOBILE*/

        if($('#qcctb .prateleira > ul')){
            $('section#qcctb h2').wrapInner('<div/>');

            $('section#qcctb .prateleira > ul').slick({
                arrows: true,
                dots: false,
                fade: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                autoplay: true,
                autoplaySpeed: 8000,
                pauseOnHover: true,
                responsive: [
                    {
                      breakpoint: 900,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true
                      }
                    },
                    {
                      breakpoint: 600,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 360,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                ]
            });
            $('#qcctb').fadeIn();
        }

        /*SKU EM GRADE*/

        $('section#area-a .right .wrapper.grade .buyProduct .uniteValue span').html('0');
        $('section#area-a .right .wrapper.grade .buyProduct .priceUnite span').html('$'+formatReal(Math.round(skuJson.skus[0].bestPrice)));

        function formatReal(int){
            var tmp = int+'';
            var neg = false;
            if(tmp.indexOf("-") == 0)
            {
                neg = true;
                tmp = tmp.replace("-","");
            }
            
            if(tmp.length == 1) tmp = "0"+tmp

            tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
            if( tmp.length > 6)
                tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
            
            if( tmp.length > 9)
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");

            if( tmp.length > 12)
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
            
            if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","");
            if(tmp.indexOf(",") == 0) tmp = tmp.replace(",","0,");

            return (neg ? '-'+tmp : tmp);
        }
        function somaInputs(){
            window.thisSoma = 0;
            $('section#area-a .right .wrapper.grade .allLines .line .sizes .item input.active.qtySelect').each(function(){
                var thisInputVal = $(this).val();
                //console.log('thisInputVal: ',thisInputVal);

                window.thisSoma = parseInt(window.thisSoma)+parseInt(thisInputVal);
                //console.log('thisSoma: ',window.thisSoma);
                $('section#area-a .right .wrapper.grade .buyProduct .uniteValue span').html(window.thisSoma);

                var priceRefresh = skuJson.skus[0].bestPrice*window.thisSoma;
                $('section#area-a .right .wrapper.grade .buyProduct .priceUnite span').html('$'+formatReal(Math.round(priceRefresh)));
            });
        }

        $('section#area-a .right .wrapper.grade .allLines .line .sizes .item input.active').live('keyup',function() {
            $(this).val(this.value.replace(/\D/g, ''));

            if($(this).val() > $(this).attr('qty')){
                $(this).val($(this).attr('qty'));
            }

            if($(this).val() > '0'){
                $(this).addClass('qtySelect');
            }else{
                $(this).removeClass('qtySelect');
            }

            somaInputs();
        });

        if(skuJson.dimensionsMap.Color){
            for(var i = 0; i < skuJson.dimensionsMap.Color.length; i++) {
                var thisColor = skuJson.dimensionsMap.Color[i];
                var thisImage = skuJson.skus[i].image;
                var thisImage = thisImage.replace('500-500','42-42');

                //$('#gridBuy .allLines').prepend('<div class="line color" color="'+thisColor+'"><div class="thumb"><i><img alt="'+thisColor+'" src="'+thisImage+'"/></i><span>'+thisColor+'</span></div><div class="sizes"></div></div>');
                $('#gridBuy .allLines').prepend('<div class="line color" color="'+thisColor+'"><div class="thumb"><button class="'+thisColor+'"></button><span>'+thisColor+'</span></div><div class="sizes"></div></div>');
            }
        }
        /*FIM COLORS*/

        for(var y = 0; y < skuJson.dimensionsMap.Size.length; y++) {
            var thisSize = skuJson.dimensionsMap.Size[y];

            $('#gridBuy .headGridList').prepend('<div class="colum size" size="'+thisSize+'"><span>'+thisSize+'</span></div>');

            $('#gridBuy .allLines .line .sizes').prepend('<div class="item" size="'+thisSize+'"></div>');
        }/*FIM SIZE*/

        for(var w = 0; w < skuJson.skus.length; w++) {
            var thisSku = skuJson.skus[w].sku;
            var thisAvailable = skuJson.skus[w].available;
            var thisAvailablequantity = skuJson.skus[w].availablequantity;
            var thisAvailablequantityORIGIN = thisAvailablequantity;
            var thisDimensionsColors = skuJson.skus[w].dimensions.Color;
            var thisDimensionsSize = skuJson.skus[w].dimensions.Size;
            var thisSellerID = skuJson.skus[w].sellerId;

            if(thisAvailablequantity > 100){
                var thisAvailablequantity = '> 100';
            }

            if(thisAvailable == false){
                $('#gridBuy .allLines .line[color="'+thisDimensionsColors+'"] .sizes .item[size="'+thisDimensionsSize+'"]').html('<input sellerId="'+thisSellerID+'" class="disabled" title="Out of stock" placeholder="-" qty="'+thisAvailablequantityORIGIN+'" type="text" thisSku="'+thisSku+'" disabled/>');
            }else{
                $('#gridBuy .allLines .line[color="'+thisDimensionsColors+'"] .sizes .item[size="'+thisDimensionsSize+'"]').html('<div class="tooltip">Quantity in<br/>stock '+thisAvailablequantity+'</div></span><input sellerId="'+thisSellerID+'" class="active" placeholder="0" qty="'+thisAvailablequantityORIGIN+'" type="text" thisSku="'+thisSku+'" />');
            }
        }/*FIM SKU*/

        $('section#area-a .right .wrapper.grade .allLines .line .sizes .item input').live('mouseover',function(){
            $(this).focus();
        });

        $('section#area-a .right .wrapper.grade .buyProduct .bts button.bt').live('click',function(){
            var _thisBt = $(this);
            if($('section#area-a .right .wrapper.grade .allLines .line .sizes .item input.active.qtySelect').size() == 0){
                alert('Please enter the quantities in the table!');
            }else{
                var thisInputsSelect = [];
                $('section#area-a .right .wrapper.grade .allLines .line .sizes .item input.active.qtySelect').each(function(){
                    var thisIDSku = $(this).attr('thissku');
                    var thisSellerID = $(this).attr('sellerid');
                    var thisQtySku = $(this).val();

                    //thisInputsSelect.push('&sku='+thisIDSku+'&qty='+thisQtySku+'&seller=1');

                    thisInputsSelect.push('&sku='+thisIDSku+'&qty='+thisQtySku+'&seller='+thisSellerID+'');
                });

                var thisInputsSelect = thisInputsSelect.join('');

                if(_thisBt.hasClass('addToBag')){//continuar comprando
                    _thisBt.find('span').html('Loading...');

                    var thisInputsSelect = '/checkout/cart/add?'+thisInputsSelect+'&redirect=false&sc=1';

                    $('#ajaxBusy').fadeIn();

                    $.ajax({
                        url: thisInputsSelect
                    }).success(function() {
                        setTimeout(function() {
                            vtexjs.checkout.getOrderForm();
                            $('#ajaxBusy').fadeOut();
                            _thisBt.find('span').html('Add to Bag');
                            alert('Products added to cart successfully!');
                            helper.fillCart();
                        },1000);
                    });

                }else{//Ir direto para o carrinho
                    _thisBt.find('span').html('Loading...');

                    var thisInputsSelect = '/checkout/cart/add?'+thisInputsSelect+'&redirect=true&sc=1';

                    $('#ajaxBusy').fadeIn();
                    window.location = thisInputsSelect;
                }   
                //console.log('thisInputsSelect: ',thisInputsSelect);    
            }
        });

        $('section#area-a .right .wrapper.grade .allLines .line .thumb button').live('click',function(){
            var thisColor = $(this).attr('class');
            //console.log('thisColor: ',thisColor);

            $('section#area-a .right .wrapper.sku ul.Color li input[data-value='+thisColor+']').click();
        });
        /*FIM SKU EM GRADE*/
    };

	if ( $('body').hasClass("catalog")) {
		catalog.init();
	};

	if ($('body').hasClass("institutional")) {
		institutional.init();
	};

    if($('body').hasClass('pag401')){
        /*** mask input ***/
        eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(5($){$.K.w=5(b,c){2(3.7==0)6;2(14 b==\'15\'){c=(14 c==\'15\')?c:b;6 3.L(5(){2(3.M){3.N();3.M(b,c)}v 2(3.17){4 a=3.17();a.1x(O);a.1y(\'P\',c);a.18(\'P\',b);a.1z()}})}v{2(3[0].M){b=3[0].1A;c=3[0].1B}v 2(Q.R&&Q.R.19){4 d=Q.R.19();b=0-d.1C().18(\'P\',-1D);c=b+d.1E.7}6{t:b,S:c}}};4 q={\'9\':"[0-9]",\'a\':"[A-T-z]",\'*\':"[A-T-1a-9]"};$.1b={1F:5(c,r){q[c]=r}};$.K.U=5(){6 3.1G("U")};$.K.1b=5(m,n){n=$.1H({C:"1I",V:B},n);4 o=D W("^"+$.1J(m.1c(""),5(c,i){6 q[c]||((/[A-T-1a-9]/.1d(c)?"":"\\\\")+c)}).1e(\'\')+"$");6 3.L(5(){4 d=$(3);4 f=D 1f(m.7);4 g=D 1f(m.7);4 h=u;4 j=u;4 l=B;$.L(m.1c(""),5(i,c){g[i]=(q[c]==B);f[i]=g[i]?c:n.C;2(!g[i]&&l==B)l=i});5 X(){x();y();1g(5(){$(d[0]).w(h?m.7:l)},0)};5 Y(e){4 a=$(3).w();4 k=e.Z;j=(k<16||(k>16&&k<10)||(k>10&&k<1h));2((a.t-a.S)!=0&&(!j||k==8||k==1i)){E(a.t,a.S)}2(k==8){11(a.t-->=0){2(!g[a.t]){f[a.t]=n.C;2($.F.1K){s=y();d.G(s.1j(0,a.t)+" "+s.1j(a.t));$(3).w(a.t+1)}v{y();$(3).w(1k.1l(l,a.t))}6 u}}}v 2(k==1i){E(a.t,a.t+1);y();$(3).w(1k.1l(l,a.t));6 u}v 2(k==1L){E(0,m.7);y();$(3).w(l);6 u}};5 12(e){2(j){j=u;6(e.Z==8)?u:B}e=e||1M.1N;4 k=e.1O||e.Z||e.1P;4 a=$(3).w();2(e.1Q||e.1R){6 O}v 2((k>=1h&&k<=1S)||k==10||k>1T){4 p=13(a.t-1);2(p<m.7){2(D W(q[m.H(p)]).1d(1m.1n(k))){f[p]=1m.1n(k);y();4 b=13(p);$(3).w(b);2(n.V&&b==m.7)n.V.1U(d)}}}6 u};5 E(a,b){1o(4 i=a;i<b&&i<m.7;i++){2(!g[i])f[i]=n.C}};5 y(){6 d.G(f.1e(\'\')).G()};5 x(){4 a=d.G();4 b=l;1o(4 i=0;i<m.7;i++){2(!g[i]){f[i]=n.C;11(b++<a.7){4 c=D W(q[m.H(i)]);2(a.H(b-1).1p(c)){f[i]=a.H(b-1);1V}}}}4 s=y();2(!s.1p(o)){d.G("");E(0,m.7);h=u}v h=O};5 13(a){11(++a<m.7){2(!g[a])6 a}6 m.7};d.1W("U",5(){d.I("N",X);d.I("1q",x);d.I("1r",Y);d.I("1s",12);2($.F.1t)3.1u=B;v 2($.F.1v)3.1X(\'1w\',x,u)});d.J("N",X);d.J("1q",x);d.J("1r",Y);d.J("1s",12);2($.F.1t)3.1u=5(){1g(x,0)};v 2($.F.1v)3.1Y(\'1w\',x,u);x()})}})(1Z);',62,124,'||if|this|var|function|return|length||||||||||||||||||||||begin|false|else|caret|checkVal|writeBuffer|||null|placeholder|new|clearBuffer|browser|val|charAt|unbind|bind|fn|each|setSelectionRange|focus|true|character|document|selection|end|Za|unmask|completed|RegExp|focusEvent|keydownEvent|keyCode|32|while|keypressEvent|seekNext|typeof|number||createTextRange|moveStart|createRange|z0|mask|split|test|join|Array|setTimeout|41|46|substring|Math|max|String|fromCharCode|for|match|blur|keydown|keypress|msie|onpaste|mozilla|input|collapse|moveEnd|select|selectionStart|selectionEnd|duplicate|100000|text|addPlaceholder|trigger|extend|_|map|opera|27|window|event|charCode|which|ctrlKey|altKey|122|186|call|break|one|removeEventListener|addEventListener|jQuery'.split('|'),0,{}))
        $('form input[name="phone"]').mask("(999)999-9999");
        $('form input[name="businessPhone"]').mask("(999)999-9999");

        $.ajax({
            type: 'GET',
            url: '/no-cache/profileSystem/getProfile',
            success: function(data) {
                //console.log(data);

                if (data.IsUserDefined != false) {
                    if (data.FirstName != null) {
                        $.ajax({
                            url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/CL/search',
                            type: 'GET',
                            dataType: 'json',
                            data: {"_fields": "id,email,approved,firstName","_where": "email="+data.Email},
                            crossDomain: true,
                            headers: {
                                'Accept': 'application/vnd.vtex.ds.v10+json',
                                'Content-Type': 'application/json',
                                'REST-Range': 'resources=0-2'
                            },
                            success: function(data) {
                                //console.log('data: ',data);
                                var fistName = data[0].firstName;
                                $('.top.cadastrado h2').html('Hello, '+fistName+'!').fadeIn('fast');
                                $('.top.cadastrado button.bt').hide();

                                if(data[0].approved == true){
                                    $('.top.cadastrado h2').after('<p>Your registration has already been approved!<br/>Click the link below and go to the homepage.</p><a href="/" class="bt gohome">HOMEPAGE</a>');
                                }else{
                                    $('.top.cadastrado h2').after('<p>Your registration has not yet been approved, please wait a little longer.<br/>You will receive an email notifying you of the approval.</p><a href="/" class="bt gohome">HOMEPAGE</a>');
                                }
                            }
                        })
                    }else{
                        $('.top.cadastrado h2').html('Hello, '+data.Email+'!').fadeIn('fast');
                        $('.top.cadastrado h2').after('<p>Please register in the form below and wait for approval by email.</p><a href="/" class="bt gohome">HOMEPAGE</a>');
                        $('.top.cadastrado button.bt').hide();
                    }
                }else{
                    $('.top.cadastrado h2').fadeIn('fast');
                    $('.top.cadastrado button.bt').fadeIn('fast');
                }
            }
        });

        $('button#login').live('click',function(){
            vtexid.start();
        });

        $('.wrapSelect select').on('change',function(){
            var thisSelect = $('option:selected', this).text();
            $(this).parents('.wrapSelect').find('.selectFake span').html(thisSelect);
        });

        $('form.form401 button.enviar').live('click', function() {
            var firstName = $('form.form401 input[name="firstName"]').val();
            var lastName = $('form.form401 input[name="lastName"]').val();
            var email = $('form.form401 input[name="email"]').val();
            var phoneNew = $('form.form401 input[name="phone"]').val();

            var corporateName = $('form.form401 input[name="corporateName"]').val();    
            var corporateDocument = $('form.form401 input[name="corporateDocument"]').val();    
            var website = $('form.form401 input[name="website"]').val();    
            var businessPhone = $('form.form401 input[name="businessPhone"]').val();    
            //var corporateName = $('form.form401 input[name="corporateName"]').val();    
            var storetype = $('form.form401 .selectFake span').text();
            
            if (firstName == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The FIRST NAME field must be filled in!</span>");
                $('form.form401 input[name="firstName"]').addClass('error');
                $('form.form401 input[name="firstName"]').focus();
                return false;
            }
            if (lastName == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The LAST NAME field must be filled in!</span>");
                $('form.form401 input[name="lastName"]').addClass('error');
                $('form.form401 input[name="lastName"]').focus();
                return false;
            }
            if (email == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The EMAIL field must be filled in!</span>");
                $('form.form401 input').removeClass('error');
                $('form.form401 input[name="email"]').addClass('error');
                $('form.form401 input[name="email"]').focus();
                return false;
            }
            var parte1 = email.indexOf("@");
            var parte3 = email.length;
            if (!(parte1 >= 3 && parte3 >= 9)) {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The EMAIL field must be valid!</span>");
                $('form.form401 input').removeClass('error');
                $('form.form401 input[name="email"]').addClass('error');
                $('form.form401 input[name="email"]').focus();
                return false;
            }
            if (phoneNew == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The PHONE field must be filled in!</span>");
                $('form.form401 input[name="phone"]').addClass('error');
                $('form.form401 input[name="phone"]').focus();
                return false;
            }

            if (corporateName == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The COMPANY NAME field must be filled in!</span>");
                $('form.form401 input[name="corporateName"]').addClass('error');
                $('form.form401 input[name="corporateName"]').focus();
                return false;
            }
            if (corporateDocument == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The EIN field must be filled in!</span>");
                $('form.form401 input[name="corporateDocument"]').addClass('error');
                $('form.form401 input[name="corporateDocument"]').focus();
                return false;
            }
            if (website == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The WEBSITE field must be filled in!</span>");
                $('form.form401 input[name="website"]').addClass('error');
                $('form.form401 input[name="website"]').focus();
                return false;
            }
            if (businessPhone == "") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>The PHONE COMPANY field must be filled in!</span>");
                $('form.form401 input[name="businessPhone"]').addClass('error');
                $('form.form401 input[name="businessPhone"]').focus();
                return false;
            }
            if (storetype == "-") {
                $('form.form401 .mensagem').fadeIn().html("<span style='color: red'>STORE TYPE field must be selected!</span>");
                $('form.form401 input').removeClass('error');
                $('form.form401 .selectFake').addClass('error');
                $('form.form401 .selectFake').focus();
                return false;
            }
            $('form.form401 .mensagem').fadeIn().html("Loading...");

            $.ajax({
                url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/CL/documents',
                dataType: 'json',
                type: 'POST',
                crossDomain: true,
                data: '{"firstName":"' + firstName + '","lastName":"' + lastName + '","email":"' + email + '","phoneNew":"' + phoneNew + '","corporateName":"' + corporateName + '","corporateDocument":"' + corporateDocument + '","website":"' + website + '","businessPhone":"' + businessPhone + '","corporateName":"' + corporateName + '","storetype":"' + storetype + '","isCorporate":"true","isNewsletterOptIn":"true"}',
                headers: {
                    'Accept': 'application/vnd.vtex.ds.v10+json',
                    'Content-Type': 'application/json; charset=utf-8'
                },
                success: function(data) {
                    $('form textarea').removeClass('error');
                    $('form.form401 .mensagem').fadeIn().html("<span style='color: green'>Pre-registration was successful. Please wait for your approval.</span>");

                    setTimeout(function(){
                        $('form.form401 .mensagem').hide();
                        $('form.form401 .mensagem').html("Loading...");
                    },6000)
                    
                    $('form input').val("");
                    $('form textarea').val("");
                }
            });

            return false;
        })
    }

    if ($('body').hasClass("institucional")) {

        $('.sidebar .wrapper-itens select').on('change', function () {
            var url = $(this).val();
            var text = $('option:selected', this).text();
            if(url) {
                $('.sidebar .wrapper-itens .selecionado').html(text);
                window.location = url;
            }
            return false;
        });

        $('.sidebar .wrapper-itens select option.'+window.location.pathname.split('/')[2]+'').attr('selected','selected');
        $('.sidebar .wrapper-itens .selecionado').html($('.wrapperContent h2').html());
        
        if($('body').hasClass('contact-us')){

            $('.wrapSelect select').on('change',function(){
                var thisSelect = $('option:selected', this).text();
                $(this).parents('.wrapSelect').find('.selectFake span').html(thisSelect);
            });

            $('.wrapperContent form button.enviar').live('click', function() {
                var name = $('.wrapperContent form input[name="name"]').val();
                var email = $('.wrapperContent form input[name="email"]').val();
                var phone = $('.wrapperContent form input[name="phone"]').val();
                var subject = $('.wrapperContent form .selectFake span').text();
                var message = $('.wrapperContent form textarea[name="message"]').val();
                
                if (name == "") {
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>The NAME field must be filled in!</span>");
                    $('.wrapperContent form input[name="name"]').addClass('error');
                    $('.wrapperContent form input[name="name"]').focus();
                    return false;
                }
                if (email == "") {
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>The EMAIL field must be filled in!</span>");
                    $('.wrapperContent form input').removeClass('error');
                    $('.wrapperContent form input[name="email"]').addClass('error');
                    $('.wrapperContent form input[name="email"]').focus();
                    return false;
                }
                var parte1 = email.indexOf("@");
                var parte3 = email.length;
                if (!(parte1 >= 3 && parte3 >= 9)) {
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>The EMAIL field must be valid!</span>");
                    $('.wrapperContent form input').removeClass('error');
                    $('.wrapperContent form input[name="email"]').addClass('error');
                    $('.wrapperContent form input[name="email"]').focus();
                    return false;
                }
                if (phone == "") {
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>The PHONE field must be filled in!</span>");
                    $('.wrapperContent form input[name="phone"]').addClass('error');
                    $('.wrapperContent form input[name="phone"]').focus();
                    return false;
                }
                if (subject == "Select") {
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>SUBJECT field must be selected!</span>");
                    $('.wrapperContent form input').removeClass('error');
                    $('.wrapperContent form .selectFake').addClass('error');
                    $('.wrapperContent form .selectFake').focus();
                    return false;
                }
                if (message == "") {
                    $('.wrapperContent .mensagem').fadeIn().html("<span style='color: red'>The MESSAGE field must be filled in!</span>");
                    $('.wrapperContent form .selectFake').removeClass('error');
                    $('.wrapperContent form input').removeClass('error');
                    $('.wrapperContent form textarea[name="message"]').addClass('error');
                    $('.wrapperContent form textarea[name="message"]').focus();
                    return false;
                }
                $('.wrapperContent .mensagem').fadeIn().html("Loading...");
                $.ajax({
                    url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/CU/documents',
                    dataType: 'json',
                    type: 'POST',
                    crossDomain: true,
                    data: '{"name":"' + name + '","email":"' + email + '","phone":"' + phone + '","subject":"' + subject + '","message":"' + message + '"}',
                    headers: {
                        'Accept': 'application/vnd.vtex.ds.v10+json',
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    success: function(data) {
                        $('form textarea').removeClass('error');
                        $('.wrapperContent .mensagem').fadeIn().html("<span style='color: green'>Sent with success!</span>");

                        setTimeout(function(){
                            $('.wrapperContent .mensagem').hide();
                            $('.wrapperContent .mensagem').html("Loading...");
                        },6000)
                        
                        $('form input').val("");
                        $('form textarea').val("");
                    }
                });

                return false;
            })
        }

    };

    if($('body').hasClass('approve')){
        $.ajax({
            url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/CL/search',
            type: 'GET',
            dataType: 'json',
            data: {"_fields": "id,email,approved,firstName","_where": "email="+window.location.search.split('?email=')[1]},
            crossDomain: true,
            headers: {
                'Accept': 'application/vnd.vtex.ds.v10+json',
                'Content-Type': 'application/json',
                'REST-Range': 'resources=0-2'
            },
            success: function(data){
                //console.log('data: ',data);
                var thisID = data[0].id;

                if(data[0].approved != true){
                    $.ajax({
                        url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/CL/documents/'+thisID+'',
                        dataType: 'json',
                        type: 'PATCH',
                        crossDomain: true,
                        data: '{"approved":"true"}',
                        headers: {
                            'Accept': 'application/vnd.vtex.ds.v10+json',
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                        success:function(dataID){
                            alert('Cliente APROVADO!');
                        }
                    });
                }else{
                    alert('Cliente já aprovado anteriormente!');
                }
            }
        })
    }

    //Página ACCOUNT
    if ($('body').hasClass('account')) {

        !function(o){"object"==typeof module&&"object"==typeof module.exports?o(require("jquery"),window,document):o(jQuery,window,document)}(function(o,t,e,i){var s=[],l=function(){return s.length?s[s.length-1]:null},n=function(){var o,t=!1;for(o=s.length-1;o>=0;o--)s[o].$blocker&&(s[o].$blocker.toggleClass("current",!t).toggleClass("behind",t),t=!0)};o.modal=function(t,e){var i,n;if(this.$body=o("body"),this.options=o.extend({},o.modal.defaults,e),this.options.doFade=!isNaN(parseInt(this.options.fadeDuration,10)),this.$blocker=null,this.options.closeExisting)for(;o.modal.isActive();)o.modal.close();if(s.push(this),t.is("a"))if(n=t.attr("href"),/^#/.test(n)){if(this.$elm=o(n),1!==this.$elm.length)return null;this.$body.append(this.$elm),this.open()}else this.$elm=o("<div>"),this.$body.append(this.$elm),i=function(o,t){t.elm.remove()},this.showSpinner(),t.trigger(o.modal.AJAX_SEND),o.get(n).done(function(e){if(o.modal.isActive()){t.trigger(o.modal.AJAX_SUCCESS);var s=l();s.$elm.empty().append(e).on(o.modal.CLOSE,i),s.hideSpinner(),s.open(),t.trigger(o.modal.AJAX_COMPLETE)}}).fail(function(){t.trigger(o.modal.AJAX_FAIL);var e=l();e.hideSpinner(),s.pop(),t.trigger(o.modal.AJAX_COMPLETE)});else this.$elm=t,this.$body.append(this.$elm),this.open()},o.modal.prototype={constructor:o.modal,open:function(){var t=this;this.block(),this.options.doFade?setTimeout(function(){t.show()},this.options.fadeDuration*this.options.fadeDelay):this.show(),o(e).off("keydown.modal").on("keydown.modal",function(o){var t=l();27==o.which&&t.options.escapeClose&&t.close()}),this.options.clickClose&&this.$blocker.click(function(t){t.target==this&&o.modal.close()})},close:function(){s.pop(),this.unblock(),this.hide(),o.modal.isActive()||o(e).off("keydown.modal")},block:function(){this.$elm.trigger(o.modal.BEFORE_BLOCK,[this._ctx()]),this.$body.css("overflow","hidden"),this.$blocker=o('<div class="jquery-modal blocker current"></div>').appendTo(this.$body),n(),this.options.doFade&&this.$blocker.css("opacity",0).animate({opacity:1},this.options.fadeDuration),this.$elm.trigger(o.modal.BLOCK,[this._ctx()])},unblock:function(t){!t&&this.options.doFade?this.$blocker.fadeOut(this.options.fadeDuration,this.unblock.bind(this,!0)):(this.$blocker.children().appendTo(this.$body),this.$blocker.remove(),this.$blocker=null,n(),o.modal.isActive()||this.$body.css("overflow",""))},show:function(){this.$elm.trigger(o.modal.BEFORE_OPEN,[this._ctx()]),this.options.showClose&&(this.closeButton=o('<a href="#close-modal" rel="modal:close" class="close-modal '+this.options.closeClass+'">'+this.options.closeText+"</a>"),this.$elm.append(this.closeButton)),this.$elm.addClass(this.options.modalClass).appendTo(this.$blocker),this.options.doFade?this.$elm.css("opacity",0).show().animate({opacity:1},this.options.fadeDuration):this.$elm.show(),this.$elm.trigger(o.modal.OPEN,[this._ctx()])},hide:function(){this.$elm.trigger(o.modal.BEFORE_CLOSE,[this._ctx()]),this.closeButton&&this.closeButton.remove();var t=this;this.options.doFade?this.$elm.fadeOut(this.options.fadeDuration,function(){t.$elm.trigger(o.modal.AFTER_CLOSE,[t._ctx()])}):this.$elm.hide(0,function(){t.$elm.trigger(o.modal.AFTER_CLOSE,[t._ctx()])}),this.$elm.trigger(o.modal.CLOSE,[this._ctx()])},showSpinner:function(){this.options.showSpinner&&(this.spinner=this.spinner||o('<div class="'+this.options.modalClass+'-spinner"></div>').append(this.options.spinnerHtml),this.$body.append(this.spinner),this.spinner.show())},hideSpinner:function(){this.spinner&&this.spinner.remove()},_ctx:function(){return{elm:this.$elm,$blocker:this.$blocker,options:this.options}}},o.modal.close=function(t){if(o.modal.isActive()){t&&t.preventDefault();var e=l();return e.close(),e.$elm}},o.modal.isActive=function(){return s.length>0},o.modal.getCurrent=l,o.modal.defaults={closeExisting:!0,escapeClose:!0,clickClose:!0,closeText:"Close",closeClass:"",modalClass:"modal",spinnerHtml:null,showSpinner:!0,showClose:!0,fadeDuration:null,fadeDelay:1},o.modal.BEFORE_BLOCK="modal:before-block",o.modal.BLOCK="modal:block",o.modal.BEFORE_OPEN="modal:before-open",o.modal.OPEN="modal:open",o.modal.BEFORE_CLOSE="modal:before-close",o.modal.CLOSE="modal:close",o.modal.AFTER_CLOSE="modal:after-close",o.modal.AJAX_SEND="modal:ajax:send",o.modal.AJAX_SUCCESS="modal:ajax:success",o.modal.AJAX_FAIL="modal:ajax:fail",o.modal.AJAX_COMPLETE="modal:ajax:complete",o.fn.modal=function(t){return 1===this.length&&new o.modal(this,t),this},o(e).on("click.modal",'a[rel~="modal:close"]',o.modal.close),o(e).on("click.modal",'a[rel~="modal:open"]',function(t){t.preventDefault(),o(this).modal()})})

        $("#editar-perfil").css("display","none");
        $("#address-edit").css("display","none");
        $("#address-remove").css("display","none");


        $("#edit-data-link").attr({rel: "modal:open"});
        $(".address-update").attr({rel: "modal:open"});
        $('#address-update').each(function () {
            $(this).attr({rel: "modal:open"})
        });
        $(".delete").each(function () {
            $(this).attr({rel: "modal:open"})
        });
        $('.modal-footer').each(function () {
            $(this).append('<a href="#close-modal" rel="modal:close" class="btn-link">Cancelar</a>');
        })

        $('.bread-crumb ul').append('<li class="last"><strong><a href="/">Minha Conta</a></strong></li>')
    } //FIM Página ACCOUNT

    /*MINICART*/
    var settings = {
        effect: 'overlay'
    };

    var cart = null;
    var helper = {
        openCart : function(){
            var width = $(cart).width() * -1;

            if(settings.effect == "push"){
                $(settings.wrapper).animate({
                    marginLeft:width
                });
            }

            $(cart).animate({
                right:0
            });

            $('.sta-cart-overlay').fadeIn();
            $('body').addClass('menuOpen');
        },
        closeCart : function(){
            var width = $(cart).width() * -1;

            if(settings.effect == "push"){
                $(settings.wrapper).animate({
                    marginLeft:0
                });
            }

            $(cart).animate({
                right:width - 10
            });

            $('.sta-cart-overlay').fadeOut();
            $('.sta-cart-comfirm').fadeOut();
            $('body').removeClass('menuOpen');
        },
        fillCart : function(){
            setTimeout(function() {
                vtexjs.checkout.getOrderForm().done(function(orderForm) {

                    var items = orderForm.items;
                    var itemsQtd = items.length
                    var i;

                    var totalItems;
                    var totalShipping;
                    var totalDiscount;

                    for (var i = 0; i < orderForm.totalizers.length; i++) {
                        if(orderForm.totalizers[i].id == "Items") {
                            totalItems = orderForm.totalizers[i].value;
                        } else if (orderForm.totalizers[i].id == "Shipping") {
                            totalShipping = orderForm.totalizers[i].value;
                        } else if (orderForm.totalizers[i].id == "Discounts") {
                            totalDiscount = orderForm.totalizers[i].value;
                        }
                    }

                    if (totalItems != undefined) {
                        $(cart).find('.sta-cart-sub strong').html('R$ ' + helper.toReal(totalItems));
                    } else {
                        $(cart).find('.sta-cart-sub strong').html('R$ ' + helper.toReal("000"));
                    }
                    if (totalShipping != undefined){
                        $(cart).find('.sta-cart-freight').css('display','block');
                        $(cart).find('.sta-cart-freight strong').html('R$ ' + helper.toReal(totalShipping));

                    }
                    if (totalDiscount != undefined){
                        $(cart).find('.sta-cart-discount').css('display','block');
                        $(cart).find('.sta-cart-discount strong').html('R$ ' + helper.toReal(totalDiscount));
                    }

                    $(cart).find('.sta-cart-total strong').html('R$ ' + helper.toReal(orderForm.value));

                    if (orderForm.totalizers) {}

                    var btnOpenCart = '<i class="ico-cart"></i> <span class="count">'+itemsQtd+'</span>';

                    //$('.sta-cart-title .count').text(itemsQtd);
                    //$('.openCart').html(btnOpenCart);

                    $(cart).find('.sta-cart-items > ul').html('');

                    if(items.length > 0){

                        $('.sta-cart-resume a').removeClass('disabled');

                        for(i = 0; i < items.length; i++){

                            var cartItem =   '<li>'
                                    cartItem +=  '<div class="sta-cart-pdt-image">';
                                        cartItem +=  '<img src="'+items[i].imageUrl.replace('55-55','80-80')+'" />';
                                    cartItem +=  '</div>';
                                    cartItem +=  '<div class="sta-cart-pdt-info">';
                                        cartItem +=  '<h4>'+items[i].name+'</h4> ';
                                        cartItem +=  '<div class="sta-cart-pdt-other-info">';
                                            cartItem +=  '<p>'+items[i].formattedPrice+'</p>';
                                            
                                            cartItem +=  '<div class="sta-cart-pdt-qtd">';
                                                cartItem +=  '<ul class="list-count list-count-cart" data-index="' + i + '">';
                                                    cartItem +=  '<li class="minus"><a href="#" class="qty-less"><span class="ico-minus">-</span></a></li>';
                                                    cartItem +=  '<li class="result"><input type="text" value="' + items[i].quantity + '" name="quantity" class="qty-field field" min="1" max="100" step="1" /></li>';
                                                    cartItem +=  '<li class="plus"><a href="#" class="qty-more"><span class="ico-plus">+</span></a></li>';
                                                cartItem +=  '</ul>';
                                            cartItem +=  '</div>';
                                            //cartItem +=  '<p class="listPrice">R$ '+helper.toReal(items[i].listPrice)+'</p>';
                                            cartItem +=  '<button class="remove-item" data-index="'+i+'">';
                                                cartItem +=  '<i class="fas fa-trash-alt"></i>';
                                            cartItem +=  '</button>';
                                        cartItem +=  '</div>';
                                    cartItem +=  '</div>';
                                cartItem +=  '</li>';

                            $(cart).find('.sta-cart-items > ul').append(cartItem);

                            if (items[i].listPrice == items[i].price) {
                                $('.sta-cart-items > ul > li').eq(i).find('p.listPrice').remove();
                            }

                        }

                        $('.sta-cart-title div.itens').html("("+itemsQtd+")"); 
                    } else{
                        $('.sta-cart-resume a').addClass('disabled');
                        helper.closeCart();
                    }
                });
            }, 500);
        },
        addItem : function(el){
            var urlTest = ["javascript",":","alert('Por favor, selecione o modelo desejado.');"].join('');
            var urlFinal = $(el).attr('href');
            var guardaQty = $('section#area-a .right .wrapper.botao .qty').val();
            var guardaQty = 1;
            //console.log('guardaQty: ', guardaQty);
            
            var urlFinal = urlFinal.replace('true','false').replace('qty=1','qty='+guardaQty+'');

            //console.log('urlFinal: ',urlFinal);

            if(urlFinal == urlTest){
                alert('Please select the desired model.');
                return false;
            } else {
                $('#ajaxBusy').fadeIn();
                $('.buy-button').addClass('aguarde');

                $.ajax({
                    url: urlFinal
                }).success(function() {
                    setTimeout(function() {
                        $('#ajaxBusy').fadeOut();
                        
                        helper.fillCart();
                        setTimeout(function(){
                            helper.openCart();
                            $('.buy-button').removeClass('aguarde');
                        },1000);

                        setTimeout(function(){
                            helper.closeCart();
                        },9000);
                    }, 1000);
                });

            }
        },
        changeItem: function(itemIndex, quantity) {
            //console.log('itemIndex: ',itemIndex);
            vtexjs.checkout.getOrderForm().then(function(orderForm) {
                var indiceIndex = itemIndex;
                var item = orderForm.items[indiceIndex];
                var updateItem = {
                    index: indiceIndex,
                    quantity: quantity
                };
                return vtexjs.checkout.updateItems([updateItem], null, false);
            }).done(function(orderForm) {
                //alert('Items atualizados!');
                //console.log(orderForm);
                helper.fillCart();
            });
        },
        removeItem : function(index){
            //if (confirm('Deseja realmente remover o item do carrinho?')) {
                vtexjs.checkout.getOrderForm().then(function (orderForm) {
                    var item = orderForm.items[index];
                    item.index = index;
                    return vtexjs.checkout.removeItems([item]);
                }).done(function (orderForm) {
                    helper.fillCart();
                });
            //}
        },
        toReal : function(val){
            val = val / 100;
            val = val.toFixed(2).toString().replace('.',',');
            return val;
        },
        
        modalComfirm : function () {
            $('.sta-cart-overlay').fadeIn();
            $('.sta-cart-comfirm').fadeIn();
            $('body').addClass('menuOpen');
        }
    };

    (function($) {

        $.fn.vtexcart = function(parameters) {

            var el = this;

            settings = $.extend(settings, parameters);

            var cartHtml = '<div class="sta-cart-overlay"></div>';
                cartHtml += '<div class="sta-cart-container">';
                    cartHtml += '<div class="sta-cart-title">';
                        cartHtml += '<h2>my cart <div class="itens"></div></h2>';
                        cartHtml += '<button class="sta-cart-close"></button>';
                    cartHtml += '</div>';
                    cartHtml += '<div class="sta-cart-items">';
                        cartHtml += '<ul></ul>'; // <-- carrega os itens do carrinho aqui
                    cartHtml += '</div>';
                    cartHtml += '<div class="sta-cart-resume">';
                        cartHtml += '<div class="sta-cart-wrap">';
                            // cartHtml += '<span class="sta-cart-itens"><em>Itens:</em> ';
                            //  cartHtml += '<strong><div class="itens"></div></strong>';
                            // cartHtml += '</span>';
                            cartHtml += '<span class="sta-cart-sub"><em>Subtotal</em> ';
                                cartHtml += '<strong>R$ 0,00</strong>';
                            cartHtml += '</span>';
                            cartHtml += '<span class="sta-cart-freight"><em>Frete:</em> ';
                                cartHtml += '<strong>R$ 0,00</strong>';
                            cartHtml += '</span>';
                            cartHtml += '<span class="sta-cart-discount"><em>Desconto:</em> ';
                                cartHtml += '<strong>R$ 0,00</strong>';
                            cartHtml += '</span>';
                            cartHtml += '<span class="sta-cart-total"><em>Total:</em> ';
                                cartHtml += ' <strong> R$0,00</strong>';
                            cartHtml += '</span> ';
                        cartHtml += '</div> ';
                        cartHtml += '<a class="checkoutLink payment" href="/checkout/#/cart">Checkout</a> ';
                        cartHtml += '<a class="checkoutLink cart" href="/checkout/#/cart">View shopping bag</a> ';
                    cartHtml += '</div>';
                cartHtml += '</div>';

            var miniCartHtml = '<a href="#" class="openCart"><span></span></a>';

            $(el).append(cartHtml);

            if(settings.cartButton){
                $(settings.cartButton).append(miniCartHtml);
            }

            cart = $(el).find('.sta-cart-container');

            helper.fillCart();

            //DIRECTIVES

            $(settings.buyButton).on('click', function(event){
                event.preventDefault();
                helper.addItem($(this));
            });

            $(".sta-cart-container").on("click", ".list-count-cart a", function(event) {
                event.preventDefault();
                var btnAction = $(this),
                    dataIndex = btnAction.closest(".list-count-cart").attr("data-index"),
                    qtyField = btnAction.closest(".list-count-cart").find(".qty-field"),
                    quantity = parseInt(qtyField.val(), 10) || 0,
                    qtyMin = qtyField.attr("min"),
                    qtyMax = qtyField.attr("max");
                btnAction.hasClass("qty-less") ? quantity != qtyMin && quantity -- : qtyMax > quantity && quantity++,
                helper.changeItem(dataIndex, quantity);
            }),

            $('.openCart').live('click', function(event){
                event.preventDefault();
                if (($('.sta-cart-items li').length)) {
                    helper.openCart();
                } else {
                    helper.openCart();

                    $('.sta-cart-container .sta-cart-items').html('<ul class="ops"><li>There are no items in your shopping bag.</li></ul>');
                    //window.location = window.location = 'https://'+window.location.hostname+'/checkout/#/cart';
                }
            });

            $('.sta-cart-container').on('click','.remove-item', function(){
                var index = $(this).data('index');
                helper.removeItem(index);
            });

            $('.sta-cart-resume a').on('click', function(){
                if($(this).hasClass('disabled')){
                    return false;
                }else{
                    return true;
                }
            });

            $('.sta-cart-freight button').click(function(){
                $(this).hide();
                $('.sta-cart-freight input').show();
            });
        };
    } (jQuery));
    $(function() {
        $("body").vtexcart({
            buyButton: $(".buy-button.buy-button-ref"),
            wrapper: $(".wrapper"),
            effect: "overlay",
            cartButton: $(".sta-cart")
        })
        $(document).on('keypress' , '.list-count .qty-field', function(e){
            var tecla = ( window.event ) ? event.keyCode : e.which;
            if( ( tecla > 47 && tecla < 58 ) ){
                return true;
            }else{
                if ( tecla == 8 || tecla == 0 ){
                    return true;
                }else{
                    return false;
                }
            }
        });
    });

    $('.sta-cart-overlay, .sta-cart-container button.sta-cart-close').live('click',function(){
        helper.closeCart();
    });
});

$(window).load(function() {

    /*submenu marcas*/
    jQuery.fn.extend({
        splitListMany: function(cols){
            var list = $(this);
            var listLen = $(this).length;
            var colSize;
            var columns;
             
            if ((cols == null)||(cols <= 0)||(columns >= listLen)) { columns = 2; }
            else if (cols >= (listLen/2)) { columns = Math.floor(listLen/2); }
            else { columns = cols; }
             
            if (listLen%columns > 0) { colSize = Math.ceil(listLen/columns); }
            else { colSize = listLen/columns; }
             
            for(var i=1; i <= columns; i++){
                list.slice((i-1)*colSize,i*colSize).wrapAll('<div class="lists list-'+i+'">');
            }
        return $(this);
        }
    });

    $.ajax({
        type: 'GET',
        url: '/api/catalog_system/pub/brand/list',
        success: function(data) {
            //console.log('data: ',data);

            for(var y = 0; y < data.length; y++) {
                if(data[y].isActive == true){
                    $('.navigation .menu-departamento .submenu li.designes .terceiroNivel ul').append('<li><a href="/'+data[y].name+'">'+data[y].name+'</a></li>');
                    $('.pageNav #designers.submenu ul').append('<li><a href="/'+data[y].name+'">'+data[y].name+'</a></li>');
                    //console.log('data[y].name: ',data[y].name);   
                }

                if(y+1 == data.length){
                    $('.navigation .menu-departamento .submenu#Women li.designes .terceiroNivel ul li').splitListMany(3);
                    $('.navigation .menu-departamento .submenu#Men li.designes .terceiroNivel ul li').splitListMany(3);
                    $('.navigation .menu-departamento .submenu#kids li.designes .terceiroNivel ul li').splitListMany(3);
                }
            }
        }
    });/*FIM submenu marcas*/
    

    $(window).resize(function() {
       fns.verifyWidth();
    })
    
    if($('body').hasClass('home')){
        $('.navigation .menu-departamento>h3.Women').addClass('active');
        $('.navigation .menu-departamento .submenu#Women').fadeIn();
    }

    if(!$('body').hasClass('pag401')){
        $.ajax({
            type: 'GET',
            url: '/no-cache/profileSystem/getProfile',
            success: function(data) {
                //console.log('data: ',data);

                if(data.IsUserDefined != false) {
                    $.ajax({
                        url: 'https://api.vtexcrm.com.br/shopcouture/dataentities/CL/search',
                        type: 'GET',
                        dataType: 'json',
                        data: {"_fields": "id,email,approved,firstName","_where": "email="+data.Email},
                        crossDomain: true,
                        headers: {
                            'Accept': 'application/vnd.vtex.ds.v10+json',
                            'Content-Type': 'application/json',
                            'REST-Range': 'resources=0-2'
                        },
                        success: function(dataApproved) {
                            //console.log('dataApproved: ',dataApproved);

                            if(dataApproved[0].approved == true){
                                $('body').addClass('yesPrice');
                            }else{
                                /*NAO LOGADO*/
                                $('body').addClass('noPrice');
                                $('.prateleira li a.urlProduct').attr('href', '/sistema/401');
                                $('.prateleira ul li .shelf-wrapper .wrapperConteudo .boxWrapPreceComprar .wrapperPreco .preco').remove();

                                if($('body').hasClass('produto')){
                                    // $('body div.all').html('');
                                    // window.location = "/sistema/401";
                                }
                            }
                        }
                    })
                }else{
                    /*NAO LOGADO*/
                    $('body').addClass('noPrice');
                    $('.prateleira li a.urlProduct').attr('href', '/sistema/401');
                    $('.prateleira ul li .shelf-wrapper .wrapperConteudo .boxWrapPreceComprar .wrapperPreco .preco').remove();

                    if($('body').hasClass('produto')){
                        $('body div.all').html('');
                        window.location = "/sistema/401";
                    }
                }
            }
        });
    }
});

$(document).ajaxStop(function() {
    $(".helperComplement").remove();

    //Página LOGIN
    if($('body').hasClass('login')) {
        $('#vtexIdContainer .vtexIdUI .vtexIdUI-classic-login-control').last().find('a.dead-link.pull-right').remove();
        $('#vtexIdContainer .vtexIdUI .vtexIdUI-classic-login-control').last().append('<a class="pull-right" href="/sistema/401">Don`t have a password? Register now!</a>');
    }
});
